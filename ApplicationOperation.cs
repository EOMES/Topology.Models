﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oxmes.Topology.Models
{
    public class ApplicationOperation
    {
        [Key]
        public int? ApplicationOperationId { get; set; }

        public int? OperationId { get; set; }

        public string OperationName { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(Application))]
        public int? ApplicationId { get; set; }

        [JsonIgnore]
        public Application Application { get; set; }

        public IEnumerable<Range> SupportedRanges { get; set; } = new List<Range>();
    }

    public class Range
    {
        /// <summary>
        /// Range ID
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Name of range
        /// </summary>
        public string Name { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(ApplicationOperation))]
        public int? ApplicationOperationId { get; set; }

        [JsonIgnore]
        public ApplicationOperation ApplicationOperation { get; set; }

        /// <summary>
        /// A collection of all <see cref="RangeParameter"/>'s
        /// </summary>
        public IEnumerable<RangeParameter> Parameters { get; set; } = new List<RangeParameter>();
    }

    public class RangeParameter
    {
        public int? Id { get; set; }

        /// <summary>
        /// Name of this parameter
        /// </summary>
        public string Name { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(Range))]
        public int? RangeId { get; set; }

        [JsonIgnore]
        public Range Range { get; set; }

        /// <summary>
        /// The minimum value supported
        /// </summary>
        public double? Minimum { get; set; }

        /// <summary>
        /// The maximum value supported
        /// </summary>
        public double? Maximum { get; set; }

        /// <summary>
        /// A string representation of an optional function to calculate the minimum value
        /// for this parameter, based on the value of other parameter(s) of this operation
        /// For format, see http://mathparser.org/mxparser-tutorial/simple-expressions/
        /// Note that other parameters must be included in the function with the format
        /// like: "{parameter1}*2", where parameter1 is another parameter of this range
        /// </summary>
        public string MinimumFunction { get; set; }

        /// <summary>
        /// A string representation of an optional function to calculate the maximum value
        /// for this parameter, based on the value of other parameter(s) of this operation
        /// For format, see http://mathparser.org/mxparser-tutorial/simple-expressions/
        /// Note that other parameters must be included in the function with the format
        /// like: "{parameter1}*2", where parameter1 is another parameter of this range
        /// </summary>
        public string MaximumFunction { get; set; }

        /// <summary>
        /// The default value for this parameter, used when operations don't specify a value.
        /// If null, this parameter is required, i.e., all operations _must_ specify a value
        /// for this parameter to be executable on this application
        /// </summary>
        public double? DefaultValue { get; set; }
    }
}