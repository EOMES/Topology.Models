﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Oxmes.Topology.Models
{
    public class Resource
    {
        [Key]
        public int ResourceId { get; set; }

        public string ResourceName { get; set; }

        [ForeignKey(nameof(ModuleConveyor))]
        public int? ModuleConveyorId { get; set; }

        [JsonIgnore]
        public ModuleConveyor ModuleConveyor { get; set; }

        [JsonIgnore]
        public Application Application { get; set; }

        [NotMapped]
        public int? ApplicationId { get; set; }

        [OnSerializing]
        public void OnSerialising(StreamingContext context)
        {
            if (!ApplicationId.HasValue) ApplicationId = Application?.ApplicationId;
        }
    }
}