﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace Oxmes.Topology.Models
{
    public class Topology
    {
        #region Properties

        [Key]
        public int TopologyId { get; set; }

        //[Required]
        public string TopologyName { get; set; }

        public bool? IsActive { get; set; }

        [JsonIgnore]
        [NotMapped]
        public IEnumerable<ModuleConveyor> ModuleConveyors
        {
            get => TopologyModuleConveyors?.Where(tmc => tmc.ModuleConveyor != null).Select(tmc => tmc.ModuleConveyor);
            set => TopologyModuleConveyors = value.Select(mc => new TopologyModuleConveyor { Topology = this, ModuleConveyor = mc });
        }

        #region SQL_Properties

        [JsonIgnore]
        public IEnumerable<TopologyModuleConveyor> TopologyModuleConveyors { get; set; } = new List<TopologyModuleConveyor>();

        [JsonIgnore]
        public IEnumerable<ConveyorLink> ConveyorLinks { get; set; } = new List<ConveyorLink>();

        #endregion SQL_Properties

        #region JSON_Properties

        [NotMapped]
        public int[] ConveyorIds { get; set; }

        [NotMapped]
        public IDictionary<int, IDictionary<int, int>> ConveyorLinkIds { get; set; }

        #endregion JSON_Properties

        #endregion Properties

        public void AddModule(ModuleConveyor conveyor, IDictionary<int, ModuleConveyor> goingTo = null)
        {
            (TopologyModuleConveyors as ICollection<TopologyModuleConveyor>).Add(new TopologyModuleConveyor { Topology = this, ModuleConveyor = conveyor });

            if (goingTo != null)
            {
                foreach (var gt in goingTo)
                {
                    AddConveyorLink(conveyor, gt.Value, gt.Key);
                }
            }
        }

        public void AddConveyorLink(ModuleConveyor moduleConveyor, ModuleConveyor goingTo, int onPort)
        {
            if (!ModuleConveyors.Contains(moduleConveyor)) AddModule(moduleConveyor);
            if (!ModuleConveyors.Contains(goingTo)) AddModule(goingTo);

            var link = ConveyorLinks.FirstOrDefault(cl => cl.ModuleConveyorSource == moduleConveyor) ?? new ConveyorLink { ModuleConveyorSource = moduleConveyor };
            if (!link.ModuleConveyorDestinations.Values.Contains(goingTo)) link.DestinationModuleConveyorConveyorLinks.Add(new ModuleConveyorConveyorLink { ConveyorLink = link, ModuleConveyor = goingTo, PortNumber = onPort });

            if (!ConveyorLinks.Contains(link)) (ConveyorLinks as ICollection<ConveyorLink>).Add(link);
        }

        [OnSerializing]
        public void OnSerialising(StreamingContext context)
        {
            if (TopologyModuleConveyors != null)
                ConveyorIds = ModuleConveyors.Where(m => m.ModuleConveyorId.HasValue).Select(m => m.ModuleConveyorId.Value).ToArray();

            if (ConveyorLinks != null)
            {
                ConveyorLinkIds = new Dictionary<int, IDictionary<int, int>>();
                foreach (var cl in ConveyorLinks)
                {
                    ConveyorLinkIds.Add(cl.ModuleConveyorSourceId,
                        cl.ModuleConveyorDestinations
                            .Where(m => m.Value.ModuleConveyorId.HasValue)
                            .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ModuleConveyorId.Value));
                }
            }
        }

        //TODO: Deserialise Json Properties into EF properties
    }

    public class ConveyorLink
    {
        [Key]
        public int LinkId { get; set; }

        [ForeignKey(nameof(ModuleConveyorSource))]
        public int ModuleConveyorSourceId { get; set; }
        public ModuleConveyor ModuleConveyorSource { get; set; }

        [NotMapped]
        public IDictionary<int, ModuleConveyor> ModuleConveyorDestinations
        {
            get => DestinationModuleConveyorConveyorLinks
                .Where(mccl => mccl.ModuleConveyor != null)
                .Select(mccl => new { mccl.PortNumber, mccl.ModuleConveyor })
                .ToDictionary(t => t.PortNumber, t => t.ModuleConveyor);
            set => DestinationModuleConveyorConveyorLinks = value
                .Select(keyValue => new ModuleConveyorConveyorLink { ConveyorLink = this, ModuleConveyor = keyValue.Value, PortNumber = keyValue.Key })
                .ToList();
        }

        public ICollection<ModuleConveyorConveyorLink> DestinationModuleConveyorConveyorLinks { get; set; } = new List<ModuleConveyorConveyorLink>();
    }

    public class ModuleConveyorConveyorLink
    {
        public int ModuleConveyorId { get; set; }
        public ModuleConveyor ModuleConveyor { get; set; }

        public int ConveyorLinkId { get; set; }
        public ConveyorLink ConveyorLink { get; set; }

        public int PortNumber { get; set; }
    }

    public class TopologyModuleConveyor
    {
        public int TopologyId { get; set; }
        public Topology Topology { get; set; }

        public int ModuleConveyorId { get; set; }
        public ModuleConveyor ModuleConveyor { get; set; }
    }
}