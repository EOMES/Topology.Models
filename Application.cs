﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oxmes.Topology.Models
{
    public class Application
    {
        /// <summary>
        /// ID of Application
        /// </summary>
        [Key]
        public int ApplicationId { get; set; }

        /// <summary>
        /// Application name
        /// </summary>
        public string ApplicationName { get; set; }

        /// <summary>
        /// The ID of the <seealso cref="Models.Resource"/> that references this Application
        /// </summary>
        [ForeignKey(nameof(Resource))]
        public int? ResourceId { get; set; }

        [JsonIgnore]
        public Resource Resource { get; set; }

        /// <summary>
        /// A list of supported <seealso cref="ApplicationOperation"/>.
        /// </summary>
        public IEnumerable<ApplicationOperation> SupportedOperations { get; set; } = new List<ApplicationOperation>();
    }
}