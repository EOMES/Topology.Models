﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.Serialization;

namespace Oxmes.Topology.Models
{
    public class ModuleConveyor
    {
        [Key]
        public int? ModuleConveyorId { get; set; }

        public string ModuleConveyorName { get; set; }

        public string PlcMacAddressString
        {
            get => PlcMacAddress?.ToString();
            set
            {
                try
                {
                    PlcMacAddress = PhysicalAddress.Parse(value);
                }
                catch (System.Exception) { }
            }
        }

        [JsonIgnore]
        [NotMapped]
        public PhysicalAddress PlcMacAddress { get; set; }

        public string PlcIpAddressString
        {
            get;
            set;
        }

        [JsonIgnore]
        public Resource Resource { get; set; }

        [NotMapped]
        public int? ResourceId { get; set; }

        [JsonIgnore]
        public IEnumerable<TopologyModuleConveyor> Topologies { get; set; } = new List<TopologyModuleConveyor>();

        [JsonIgnore]
        public IEnumerable<ModuleConveyorConveyorLink> IncomingConveyorLinks { get; set; } = new List<ModuleConveyorConveyorLink>();

        [JsonIgnore]
        public IEnumerable<ConveyorLink> OutgoingConveyorLinks { get; set; }

        [OnSerializing]
        public void OnSerialising(StreamingContext context)
        {
            if (!ResourceId.HasValue) ResourceId = Resource?.ResourceId;
        }
    }
}